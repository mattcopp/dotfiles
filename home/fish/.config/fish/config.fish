export XCURSOR_SIZE=20
if test -z "$DISPLAY" -a $XDG_VTNR = 1
    # Start sway when at tty1
    export WM=sway
    export QT_STYLE_OVERRIDE=adwaita-dark
    export TERM=xterm
    exec sway --my-next-gpu-wont-be-nvidia -d 2> ~/.sway.log
else
    # Eyecandy
    pfetch
end
