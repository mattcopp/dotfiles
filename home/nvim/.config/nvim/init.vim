set nomodeline

set number
set relativenumber

" Sane tabbing
set tabstop=4
set softtabstop=0 expandtab
set shiftwidth=4
set smarttab

" Use system clipboard
set clipboard+=unnamedplus

" Ignore case in search
set ignorecase

" Splits
set splitbelow splitright
nnoremap <C-Up>    <C-w>Up
nnoremap <C-Down>  <C-w>Down
nnoremap <C-Left>  <C-w>Left
nnoremap <C-Right> <C-w>Right
nnoremap <C-h>     <C-w>h
nnoremap <C-j>     <C-w>j
nnoremap <C-k>     <C-w>k
nnoremap <C-l>     <C-w>l
