#!/bin/sh

# Depends on app-misc/dateutils

seconds="$(date --file '/usr/portage/metadata/timestamp.chk' '+%s')"

ddiff -i '%s' "$seconds" 'now' -f '%ww_%dd_%Hh_%Mm' | sed 's/^\(0._\)*//; s/_/ /; s/_.*//'
