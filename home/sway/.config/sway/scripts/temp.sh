#!/bin/sh

# Maximum temperature found in sensors
t="$(sensors | sed 's/(.*)//' | grep -o '[0-9]*\.[0-9]*°C' | sed 's/°C//' | sort -n | tail -n1)"

# Red color if temperature exceeds 60.0
case $(echo "$t<60.0" | bc) in
	1)
		echo "$t°C"
		;;
	*)
		echo "<span color='red'>$t°C</span>"
esac
