#!/bin/sh

normal() {
    echo "$1%"
}

red() {
    echo "<span color='red'>$1%</span>"
}

printPerc() {
    local capFile="$1/capacity"
    [ -e "$capFile" ] && {
        local cap="$(cat $capFile)"
        [ "$cap" -gt '100' ] && cap='100'
        [ "$cap" -ge '20' ] && normal "$cap" || {
            local statFile="$1/status"
            [ -e "$statFile" ] && case "$(cat $statFile)" in
                'Charging')
                    normal "$cap"
                    ;;
                'Discharging')
                    red "$cap"
                    ;;
                'Not charging')
                    red "$cap"
                    ;;
                *)
                    red "$(cat $statFile)" 
            esac
        }
    }
}

for bat in /sys/class/power_supply/BAT*
do
    printPerc "$bat"
done | paste -d' '
