#!/bin/bash

case "$BLOCK_BUTTON" in
    2)
        nmcli -t con | grep ':enp2s0$' |\
            cut -f1 -d':' | xargs nmcli con down >/dev/null
esac

nmcli -t con | grep ':enp2s0$' | cut -f1 -d':'
