#!/bin/sh

perc="$(free -b | awk '/^Mem:/ {printf "%.1f", 100*$3/$2}')"
used="$(free -h | awk '/^Mem:/ {print $3}')"

case $(echo "${perc}<80" | bc) in
	1)
		echo "${used} (${perc}%)"
		;;
	*)
		echo "<span color='red'>${used} (${perc}%)</span>"
esac
