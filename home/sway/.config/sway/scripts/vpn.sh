#!/bin/sh

case "$BLOCK_BUTTON" in
    2)
        nmcli -t con | grep ':vpn:.\+$' |\
            cut -f1 -d':' | xargs nmcli con down >/dev/null
esac

nmcli -t con | grep ':vpn:.\+$' | cut -f1 -d':'
