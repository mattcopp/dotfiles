#!/bin/sh

# CPU load in the last minute
load=$(uptime | grep -oE '[0-9]+\.[0-9]+' | head -1)

# Red color if ${load}>8.0
case $(echo "${load}>8.0" | bc) in
	0)
		echo "$load"
		;;
	*)
		echo "<span color='red'>$load</span>"
esac
