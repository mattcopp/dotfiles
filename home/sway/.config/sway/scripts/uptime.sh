#!/bin/sh

ddiff -i'%s' "$(uptime -s | date '+%s' -f '-')" -i'%s' "$(date -u '+%s')" -f '%ww_%dd_%Hh_%Mm' | sed 's/^\(0._\)*//; s/_/ /; s/_.*//'
