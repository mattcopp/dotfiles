# Default config for sway
#
# Copy this to ~/.config/sway/config and edit it to your liking.
#
# Read `man 5 sway` for a complete reference.

### Variables
#
# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left  j
set $down  k
set $up    l
set $right m
# Your preferred terminal emulator
set $term kitty 2>&1 >> .kitty.log
# Preferred browser and e-mail client
set $browser firefox
set $mail    thunderbird
# Your preferred application launcher
# Note: it's recommended that you pass the final command to sway
#set $menu bemenu-run | xargs swaymsg exec
set $menu wofi --show drun

### Output configuration
#
# Default wallpaper (more resolutions are available in /usr/share/backgrounds/sway/
output "*" bg /home/matthias/Pictures/background.jpg stretch

### Input configuration
#
input * {
    xkb_layout     be
    xkb_numlock    enabled
    tap            enabled
    scroll_method  two_finger
    tap_button_map lrm
}

### Key bindings
#
# Basics:
#
    # start a terminal
    bindsym $mod+Return exec $term

    # kill focused window
    bindsym $mod+Shift+q kill
    bindsym --release button3 kill

    # start your launcher
    bindsym $mod+d exec $menu

    # Start browser or e-mail client
    bindsym $mod+b exec $browser
    bindsym $mod+n exec $mail

    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # reload the configuration file
    bindsym $mod+Shift+r reload

    # Print screen
    bindsym Print               exec "        grim      ~/Pictures/$(date +%Y-%m-%d_%H:%M).png"
    bindsym Shift+Print         exec "slurp | grim -g - ~/Pictures/$(date +%Y-%m-%d_%H:%M).png"
    bindsym Control+Print       exec "~/.config/sway/scripts/screencast.sh full"
    bindsym Control+Shift+Print exec "~/.config/sway/scripts/screencast.sh"

    # Pulseaudio controls
    set $sink alsa_output.usb-Razer_Razer_Kraken_X_USB_00000000-00.analog-stereo
    bindsym --locked XF86AudioRaiseVolume exec "pactl set-sink-mute $sink 0 && pactl set-sink-volume $sink +5% && pkill -RTMIN+1 i3blocks"
    bindsym --locked XF86AudioLowerVolume exec "pactl set-sink-mute $sink 0 && pactl set-sink-volume $sink -5% && pkill -RTMIN+1 i3blocks"
    #bindsym --locked XF86AudioMute exec "pactl set-sink-mute $sink toggle && pkill -RTMIN+1 i3blocks"
    bindsym --locked XF86AudioMute exec "true"
    
    # Screen brightness controls
    bindsym --locked XF86MonBrightnessUp   exec "light -A 5"
    bindsym --locked XF86MonBrightnessDown exec "light -U 5"

    # Lock screen
    bindsym $mod+Shift+x exec "swaylock --screenshot --effect-blur 10x3 --clock --indicator-idle-visible --timestr '%H:%M' --datestr '%a %d-%m-%Y'"

    # Clipboard to QR-code
    bindsym $mod+q exec qr

    # exit sway (logs you out of your Wayland session)
    bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'
#
# Moving around:
#
    # Move your focus around
    bindsym $mod+$left focus left
    bindsym $mod+$down focus down
    bindsym $mod+$up focus up
    bindsym $mod+$right focus right
    # or use $mod+[up|down|left|right]
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    # _move_ the focused window with the same, but add Shift
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right
    # ditto, with arrow keys
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right
#
# Workspaces:
#
    # switch to workspace
    bindsym $mod+ampersand  workspace 1
    bindsym $mod+eacute     workspace 2
    bindsym $mod+quotedbl   workspace 3
    bindsym $mod+apostrophe workspace 4
    bindsym $mod+parenleft  workspace 5
    bindsym $mod+section    workspace 6
    bindsym $mod+egrave     workspace 7
    bindsym $mod+exclam     workspace 8
    bindsym $mod+ccedilla   workspace 9
    bindsym $mod+agrave     workspace 10
    # move focused container to workspace
    bindsym $mod+Shift+ampersand  move container to workspace 1 ; workspace 1
    bindsym $mod+Shift+eacute     move container to workspace 2 ; workspace 2
    bindsym $mod+Shift+quotedbl   move container to workspace 3 ; workspace 3
    bindsym $mod+Shift+apostrophe move container to workspace 4 ; workspace 4
    bindsym $mod+Shift+parenleft  move container to workspace 5 ; workspace 5
    bindsym $mod+Shift+section    move container to workspace 6 ; workspace 6
    bindsym $mod+Shift+egrave     move container to workspace 7 ; workspace 7
    bindsym $mod+Shift+exclam     move container to workspace 8 ; workspace 8
    bindsym $mod+Shift+ccedilla   move container to workspace 9 ; workspace 9
    bindsym $mod+Shift+agrave     move container to workspace 10; workspace 10
    # move focused container to workspace
    bindsym $mod+Control+ampersand  move container to workspace 1
    bindsym $mod+Control+eacute     move container to workspace 2
    bindsym $mod+Control+quotedbl   move container to workspace 3
    bindsym $mod+Control+apostrophe move container to workspace 4
    bindsym $mod+Control+parenleft  move container to workspace 5
    bindsym $mod+Control+section    move container to workspace 6
    bindsym $mod+Control+egrave     move container to workspace 7
    bindsym $mod+Control+exclam     move container to workspace 8
    bindsym $mod+Control+ccedilla   move container to workspace 9
    bindsym $mod+Control+agrave     move container to workspace 10
    # move workspace to output
    bindsym $mod+Control+Shift+$left  move workspace to output left
    bindsym $mod+Control+Shift+$right move workspace to output right
    bindsym $mod+Control+Shift+$up    move workspace to output up
    bindsym $mod+Control+Shift+$down  move workspace to output down
    # move workspace to output with arrow keys
    bindsym $mod+Control+Shift+Left  move workspace to output left
    bindsym $mod+Control+Shift+Right move workspace to output right
    bindsym $mod+Control+Shift+Up    move workspace to output up
    bindsym $mod+Control+Shift+Down  move workspace to output down
#
# Layout stuff:
#
    # You can "split" the current object of your focus with
    # $mod+b or $mod+v, for horizontal and vertical splits
    # respectively.
    bindsym $mod+h splith
    bindsym $mod+v splitv

    # Switch the current container between different layout styles
    bindsym $mod+s layout stacking
    bindsym $mod+w layout tabbed
    bindsym $mod+e layout toggle split

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+space floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+space focus mode_toggle

    # move focus to the parent container
    bindsym $mod+a focus parent
#
# Scratchpad:
#
    # Sway has a "scratchpad", which is a bag of holding for windows.
    # You can send windows there and get them back later.

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+minus move scratchpad

    # Show the next scratchpad window or hide the focused scratchpad window.
    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+minus scratchpad show
#
# Resizing containers:
#
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $left  resize shrink width  10px
    bindsym $down  resize grow   height 10px
    bindsym $up    resize shrink height 10px
    bindsym $right resize grow   width  10px

    # ditto, with arrow keys
    bindsym Left  resize shrink width  10px
    bindsym Down  resize grow   height 10px
    bindsym Up    resize shrink height 10px
    bindsym Right resize grow   width  10px

    # return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

set $bg-color            #171a1f
set $inactive-bg-color   #171a1f
set $text-color          #f3f4f5
set $inactive-text-color #676E7D
set $urgent-bg-color     #E53935
set $indicator-color     #00FF00

client.focused          $bg-color          $bg-color          $text-color          $indicator-color $bg-color
client.unfocused        $inactive-bg-color $inactive-bg-color $inactive-text-color $indicator-color $inactive-bg-color
client.focused_inactive $inactive-bg-color $inactive-bg-color $inactive-text-color $indicator-color $inactive-bg-color
client.urgent           $urgent-bg-color   $urgent-bg-color   $text-color          $indicator-color $urgent-bg-color

font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1 8

#
# Status Bar:
#
# Read `man 5 sway-bar` for more information about this section.
bar {
    output eDP-1
    status_command i3blocks -c ~/.config/sway/i3blocks.conf
    colors {
        background $bg-color
	separator #757575
	focused_workspace  $bg-color          $bg-color          $text-color
        inactive_workspace $inactive-bg-color $inactive-bg-color $inactive-text-color
	urgent_workspace   $urgent-bg-color   $urgent-bg-color   $text-color
    }
}
bar {
    output DP-1
    output HDMI-A-2
    status_command i3blocks -c ~/.config/sway/i3blocks2.conf
    colors {
        background $bg-color
	separator #757575
	focused_workspace  $bg-color          $bg-color          $text-color
        inactive_workspace $inactive-bg-color $inactive-bg-color $inactive-text-color
	urgent_workspace   $urgent-bg-color   $urgent-bg-color   $text-color
    }
}

hide_edge_borders both

for_window [app_id="pavucontrol"] floating enable
for_window [app_id="wdisplays"] floating enable
for_window [class="Tor Browser"] floating enable
for_window [title="UGent Functioneel Programmeren Opdracht 1"] floating enable
for_window [title="UGent Functioneel Programmeren Opdracht 2"] floating enable
for_window [title="UGent Functioneel Programmeren Opdracht 3"] floating enable
for_window [window_role="pop-up"] floating enable
for_window [window_role="bubble"] floating enable
for_window [window_role="task_dialog"] floating enable
for_window [window_role="Preferences"] floating enable
for_window [window_type="dialog"] floating enable
for_window [window_type="menu"] floating enable
for_window [window_type="notification"] floating enable
for_window [title="Open With"] floating enable
for_window [class="Pinentry"] floating enable

exec_always {
    gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'
    gsettings set org.gnome.desktop.interface icon-theme 'HighContrast'
    gsettings set org.gnome.desktop.interface cursor-theme 'Adwaita'
}
exec_always mako
exec_always dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway

include /etc/sway/config.d/*
